# @jailers-oss/internal-mde

> Internal Markdown editor

## Install

### NPM

```bash
npm install --save vue markdown-it @jailers-oss/internal-mde
```

### Yarn

```bash
yarn add vue markdown-it @jailers-oss/internal-mde
```

## Basic usage

```vue
<template>
  <div id="app">
    <markdown-editor v-model="output" locale="cs" />
  </div>
</template>

<script lang="ts">
import Vue from 'vue'
import MarkdownEditor from '@jailers-oss/internal-mde'

export default Vue.extend({
  name: 'app',

  components: {
    MarkdownEditor,
  },

  data() {
    return {
      output: '',
    }
  },
})
</script>
```

## License

MIT
