#### 0.1.2 (2019-11-18)

##### Chores

- bump version (8d46aa8d)
- bump version (4ef7af58)
- remove 'dist' (update git indexes) (b67180d4)
- update README.md (9fe4f225)
- migrate repo (5569474f)

##### New Features

- remove unnecessary title (af10a0e4)

##### Bug Fixes

- fix CI, update README (ac468e07)
- remove key duplication in .gitlab-ci.yml (930be4dd)

#### 0.1.1 (2019-11-10)

##### Chores

- remove 'dist' (update git indexes) (b67180d4)
- update README.md (9fe4f225)
- migrate repo (5569474f)

##### New Features

- remove unnecessary title (af10a0e4)

##### Bug Fixes

- remove key duplication in .gitlab-ci.yml (930be4dd)
