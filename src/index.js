import MarkdownEditor from '@/components/MarkdownEditor.vue'

function install(Vue) {
  if (install.installed) return
  install.installed = true
  Vue.component('MarkdownEditor', MarkdownEditor)
}

const plugin = {
  install,
}

/* global window global */
let GlobalVue = null
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue
}
if (GlobalVue) {
  GlobalVue.use(plugin)
}

MarkdownEditor.install = install

export default MarkdownEditor
