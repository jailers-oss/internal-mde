const en = {
  md: {
    tab: {
      editor: 'Editor',
      preview: 'Show preview',
    },
    action: {
      buttonLabel: {
        heading: 'Heading',
        link: 'Link',
        image: 'Image',
      },
      fallback: {
        heading: 'Heading text',
        link: 'Link label',
        image: 'Image label',
      },
    },
  },
}

const cs = {
  md: {
    tab: {
      editor: 'Editor',
      preview: 'Zobrazit náhled',
    },
    action: {
      buttonLabel: {
        heading: 'Nadpis',
        link: 'Odkaz',
        image: 'Obrázek',
      },
      fallback: {
        heading: 'Text nadpisu',
        link: 'Popis odkazu',
        image: 'Popis obrázku',
      },
    },
  },
}

export const i18nResources = {
  en,
  cs,
}
