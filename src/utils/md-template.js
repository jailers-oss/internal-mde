export const ActionTemplate = {
  heading: {
    template: '## %0%%c%',
    fallback: 'md.action.fallback.heading',
  },

  link: {
    template: '[%0%](http://%c%)',
    fallback: 'md.action.fallback.link',
  },

  image: {
    template: `![%0%](/%c%)`,
    fallback: 'md.action.fallback.image',
  },
}
