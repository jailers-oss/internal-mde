import { i18n } from '@/i18n'

export const translate = val => i18n.t(val)
