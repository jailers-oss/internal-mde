import get from 'lodash.get'
import { i18nResources as resources } from '@/utils'

class I18n {
  constructor(lng = 'en') {
    this.lng = lng
  }

  set locale(lng) {
    this.lng = lng
  }

  t(path) {
    return get(resources, `${this.lng}.${path}`, path)
  }
}

export const i18n = new I18n()
